////////////////////////////////////////////////////////////////////////////////
//  imports
import { normalize as normalizePath, basename } from "node:path";
import { URL } from "node:url";
import { getEntry, getBase } from "multiroot-path";

////////////////////////////////////////////////////////////////////////////////
//  Creates a server request handler with a bind removeable config start.
//    config.log:   This function will be called to output all information from 
//                  an incoming request. It defaults to a silent ignore.
//    config.roots: An array of string paths. The roots are the base of every URL 
//                  path that accesses the file system.
//    req:          Same as node's HTTP server.on("request")
//    res:          Same as node's HTTP server.on("request")
export const getPath = async (config, req, res) => {
  let { log, roots } = config;
  const requestHeaders = req.headers;
  const range = requestHeaders["range"];
  const origin = `${req.protocol}://${req.get("host")}`;
  const {pathname: unescapedPath, searchParams} = new URL(`${origin}${req.url}`);
  const path = normalizePath(unescape(unescapedPath)); 
  
  log(`${(new Date()).toJSON()} Request on ${req.protocol}://${req.get("host")}${req.path} with range = '${range}'`);
  
  const base = await getBase(roots, path);
  
  ////////////////////////////////////
  //  If an error occurs, then just short throw the error
  if (base.type === "error") {
    if (base.code === "no-entry") {
      res.sendStatus(404);
    }
    else {
      res.sendStatus(500);
    }
  }
  
  //////////////////////////////////
  //  If the request is for a directory, then response with an json 
  //    document of the directory's information
  else if (base.type === "directory") {
    const entry = await getEntry( roots,  path );
    
    res
      .type("application/json")
      .status(200)
      .send({
        type: entry.type,
        path: entry.path,
        name: entry.name,
        parentPath: entry.parentPath,
        parentName: entry.parentName,
        size: entry.size,
        sizeUnit: entry.sizeUnit,
        entries: await Promise.all(entry.children
          .map(path => getEntry( roots, path )
            .then(({path, name, size, sizeUnit, type, mimetype}) => ({type, path, name, size, sizeUnit, mimetype}))
          )
        ),
      });
  }
  
  //////////////////////////////////
  //  If the request is for the file then give it
  else if (base.type === "file") {
    const entry = await getEntry( roots, path );
  
    res.sendFile(
      base.rootedPath, 
    
      {
        headers: {
          "content-type": entry.mimetype,
        }
      },
    
      (err) => {
        if (err && typeof(err) === "object" && err !== null && err.code === "ENONENT") {
          res.sendStatus(500);
        }
      }
    );
  }
  
  ////////////////////////////////////
  //  If the entry is not a file or directory then response that the type 
  //    is unknown
  else {
    res
      .type("application/json")
      .status(500)
      .send({
        path: base.path,
        name: base.name,
        type: "error",
        reason: "Unrecognized file type",
        code: "unknown-type",
      });
  }
};
