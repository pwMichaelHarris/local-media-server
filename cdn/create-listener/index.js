////////////////////////////////////////////////////////////////////////////////
//  imports
import express from "express";
import { getPath as getHandler } from "./get.js";

////////////////////////////////////////////////////////////////////////////////
//  Creates a server request handler.
//    log:        This function will be called to output all information from 
//                an incoming request. It defaults to a silent ignore.
//    roots:      An array of string paths. The roots are the base of every URL 
//                path that accesses the file system.
export default ({ roots, log }) => {
  log = log ?? ((...args) => {});
  
  const app = express();
  
  const config = {
    roots,
    log,
  };
  
  app.get("*", getHandler.bind(undefined, config));
  app.post("/upload", (req, res) => {
    res.sendStatus(501);
  });

  return app;
};

