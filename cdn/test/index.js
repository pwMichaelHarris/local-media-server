////////////////////////////////////////////////////////////////////////////////
import createServer from "./create-server.js";
import { request as nodeRequest } from "node:http";

////////////////////////////////////////////////////////////////////////////////
const roots = [
  "/home/pw/Videos",
  "/home/pw/Music",
  "/home/pw/Pictures",
];
const port = 3001;
const address = "127.0.0.1";
const log = console.log.bind(console);

////////////////////////////////////////////////////////////////////////////////
const origin = `http://${address}:${port}`;
const paths = [
//  "/",
//  "/eevee",
//  "/junk",
//  "/no-exists",
  { path: "/upload", options: { method: "post" } },
];

////////////////////////////////////////////////////////////////////////////////
const request = (url, options) => new Promise((resolve, reject) => {
  const req = nodeRequest(url, options, (res) => {
    const { statusCode, headers } = res;
    const contentType = headers["content-type"];
    
    const buffer = [];
    res.on("data", buffer.push.bind(buffer));
    res.on("end", () => {
      let body = Buffer.concat(buffer)
      if (/application\/json(;|$)/.test(contentType)) {
        body = JSON.parse(body.toString("utf-8"));
      }
      else if (/text\/html(;|$)/.test(contentType)) {
        body = body.toString("utf-8");
      }
      
      resolve({ statusCode, contentType, headers, body });
    });
  });
  req.on("error", reject);
  req.end();
});

////////////////////////////////////////////////////////////////////////////////
const server = createServer({port, address, roots, log});

for (let path of paths) {
  try {
    let result;
    if (typeof(path) === "string") {
      result = await request(`${origin}${path}`);
    } else if (typeof(path) === "object" && path !== null) {
      result = await request(`${origin}${path.path}`, path.options);
    } else {
      throw `ERROR: path must be string or an object containing "path" and "options" fields.`;
    }
    console.log(result);
  } catch (e) {
    console.error(e);
  }
}
server.close();


