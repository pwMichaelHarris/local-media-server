////////////////////////////////////////////////////////////////////////////////
//  imports
import createServer from "./create-server.js";
import commandLineArgs from "command-line-args";
import commandLineUsage from "command-line-usage";
import { 
  isAbsolute as isAbsolutePath, 
  normalize as normalizePath 
} from "node:path";

////////////////////////////////////////////////////////////////////////////////
//  CLI options
const optionDefinitions = [
  { 
    name: "help", 
    type: Boolean, 
    description: "Display this guide." 
  }, { 
    name: "noise", 
    alias: "n", 
    type: Number, 
    defaultValue: "1", 
    description: "How much should the server output.\n0. No output\n1. Output only start up and closing and each request made" 
  }, { 
    name: "origin", 
    alias: "o", 
    type: String, 
    defaultValue: "http://127.0.0.1:3001", 
    lazyMultiple: true, 
    typeLabel: "{underline URL}",
    description: "The origin to be used for the server. Multiple server may be used for load balancing. (MULTIPLE SERVER CURRENTLY NOT IMPLEMENTED)"
  }, { 
    name: "root", 
    alias: "r", 
    type: String, 
    lazyMultiple: true, 
    typeLabel: "{underline path}",
    description: "Add a single root path for the sever to use as a base for server on the file system directory" 
  }, { 
    name: "roots", 
    type: String, 
    multiple: true,
    typeLabel: "{underline paths} ...",
    description: "Add multiple root paths" 
  },
];

const options = commandLineArgs(optionDefinitions, { camelCase: true });

////////////////////////////////////////////////////////////////////////////////
//  If help was selected, then just output the options and quit
if (options.help) {
  const usage = commandLineUsage([
    {
      header: "Options",
      optionList: optionDefinitions
    }
  ]);
  
  console.log(usage);
  process.exit(0);
}

////////////////////////////////////////////////////////////////////////////////
//  If help was not selected then build all other values.

//  Noise level. Logging is currently just tracking request coming in.
let log;
if (options.noise === "1") {
  log = console.log.bind(log);
} else if (options.noise === "0") {
  log = (...args) => {};
} else {
  console.error("ERROR: Noise level must be 0 or 1.");
  process.exit(1);
}

//  Where the server is located.
let address, port;
try {
  let url = new URL(options.origin[0]);
  address = url.hostname;
  port = Number.parseInt(url.port || "80");
}
catch (e) {
  if (e.code === "ERR_INVALID_URL") {
    console.error("ERROR: The server's origin must be a valid URL.");
    process.exit(1);
  }
  else {
    throw e;
  }
}

//  The root bases for all file system look ups
let roots = [...(options.root ?? []), ...(options.roots ?? [])];
if (roots.length === 0) {
  roots.push(process.cwd());
}
else {
  roots = roots.map(root => {
    if (isAbsolutePath(root)) {
      return root;
    }
    return normalizePath(`${process.cwd()}/${root}`);
  });
}

////////////////////////////////////////////////////////////////////////////////
const server = createServer({ log, address, port, roots });

