////////////////////////////////////////////////////////////////////////////////
//  Imports
import { createServer } from "node:http";
import createListener from "create-listener";

////////////////////////////////////////////////////////////////////////////////
//  Creates a simple node server using the listener. Set create-listener for 
//    details.
export default ({port, address, roots, log}) => {
  log = log ?? ((...args) => {});

  const listener = createListener({roots, log});
  const server = createServer(listener);

  server.on("listening", () => {
    log(`${(new Date).toJSON()} Server listening on http://${address}:${port}`);
    
    process.on("SIGINT", () => {
      server.close();
    });
  });

  server.on("close", () => {
    log(`${(new Date).toJSON()} Server on http://${address}:${port} closed`);
    process.exit();
  });

  server.listen(port, address);
  
  return server;
};
