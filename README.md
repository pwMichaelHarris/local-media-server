# Local Server

This local server will provide and file system-like experience in the web browser to allow a client to navigate a local machine's files. Currently the server allows read-only options. The media formats of video, audio, and image will be displayed via HTML5. Other media formats will be given to the browser directly with no currently defined behavior.

---

## Requirements
For development, you will only need Node.js and a node global package, NPM, installed in your environement.

The server was designed on Node.js version 18.9.0 on a Linux Mint 20.1 Machine.

## Running
### Installing and running running
```
npm i
node index.js
```
-or-
```
npm i
npm start
```

### Run options
A list of all run options are available via the *--help* command line options.
* CDN's origin: The address and port  that the client server and client should use for accessing the CDN. Currently can only be set to one URL root. Defaults to 127.0.0.1:3001.
* Origin: The address and port for the client server. Currently can only be set to one URL root. Defaults to 127.0.0.1:3000.
* Root(s): The folder from the local machine where the content can be found. Multiple roots can be selected; the server will place them under the **/fs** path of the site. If multiple folders share the same path, then the server will render their content as one unified folder of the common path. If multiple files or both a file and folder share the same sub-path relative to multiple roots, then the site will return that the path is ambiguous. Defaults to the terminal's current working directly.

### Running examples
The following example will create a content server at *http://192.168.0.2:3001* and a client server on *http://192.168.0.2:3000* with using the current directory as the content's root folder.
```
node index.js -c http://192.168.0.2:3001 -o http://192.168.0.2:3000
```
-or-
```
npm start -- -c http://192.168.0.2:3001 -o http://192.168.0.2:3000
```

The following example will create a content server at *http://127.0.0.1:3001* and a client server on *http://127.0.0.1:3000* with using the folders *videos*, *music*, and *audio* as the content roots
```
node index.js --roots videos music audio
```
-or-
```
npm start -- --roots videos music audio
```

---

## Major Sections
### client
Creates a client server to allow browser based GUI access to the content server.

### multiroot-path
Model used by the client and content server to resolve URL path's to a single file on the host machine or combine multiple folder on the host machine.

### cdn
Currently only a content server. The server provides access to the machine's files given by the roots arguments.

---

## Known Issues
* CRUD options are currently unavailable in this version due to complexity of interaction with writing and multiroot-path module.
* Server does not work with proxies
* Server provides **NO** security. **Not recommended** for production use outside of trusted local network.

