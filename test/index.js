////////////////////////////////////////////////////////////////////////////////
import { createServer } from "cdn";
import { request } from "node:http";

////////////////////////////////////////////////////////////////////////////////
const roots = [
  "/home/pw/Videos",
  "/home/pw/Music",
  "/home/pw/Pictures",
];

//const port = 3001;
//const address = "127.0.0.1";
const port = 8080;
const address = "127.0.0.1";
const origin = `http://${address}:${port}/`;
const url = `${origin}eevee`;

////////////////////////////////////////////////////////////////////////////////
const server = createServer({ roots, port: port, address, log: console.log.bind(console) });

////////////////////////////////////////////////////////////////////////////////
server.on("listening", () => {
  const req = request(
    url, 
    
    (res) => {
      const { statusCode, headers } = res;
      const contentType = headers["content-type"];
      
      if (statusCode === 200 && /application\/json(;|$)/.test(contentType)) {
        console.log((new Date).toJSON(), { statusCode, contentType });
        const buffer = [];
        res.on("data", buffer.push.bind(buffer));
        res.on("end", () => {
          const data = JSON.parse(Buffer.concat(buffer).toString("utf-8"));
          if (data.type === "directory") {
            console.log(data.entries);
          }
          server.close();
        });
      }
      else {
        console.log({ statusCode, headers });
        server.close();
      }
      
    }
  );
  req.on("error", (e) => {
    console.error(e);
    server.close();
  });
  req.end();
});

