"use strict";

const AUTOPLAY_DELAY = 3000;

window.addEventListener("DOMContentLoaded", (e) => {
  let autoplayElements = document.getElementsByClassName("js-autoplay");
  let nextElements = document.getElementsByClassName("js-next");
  let prevElements = document.getElementsByClassName("js-prev");
  let videoElements = document.getElementsByClassName("js-video");
  let audioElements = document.getElementsByClassName("js-audio");
  let imageElements = document.getElementsByClassName("js-image");
  let nameElements = document.getElementsByClassName("js-name");
  let playerElements = document.getElementsByClassName("js-player");
  
  let autoplay;
  let nextURL;
  let imageTimeout;
  
  const loadPage = async ({ autoplay, url }) => {
    clearTimeout(imageTimeout);
    let fetchURL = new URL(url);
    fetchURL.searchParams.set("request-json", true);
    let res = await fetch(fetchURL.href);
    let data = await res.json();
    
    const origin = window.location.origin;
    nextURL = `${origin}/fs/${data.nextPath}`;
    setNext({ path: `${origin}/fs/${data.nextPath}`, name: data.nextName });
    setPrev({ path: `${origin}/fs/${data.prevPath}`, name: data.prevName });
    
    for (let element of videoElements) {
      element.src = "";
    }
    for (let element of audioElements) {
      element.src = "";
    }
    for (let element of imageElements) {
      element.src = "";
    }
    
    if (autoplay) {
      enableAutoplay();
    }
    else {
      disableAutoplay();
    }
    
    document.title = data.name;
    for (let element of nameElements) {
      element.textContent = data.name;
    }
    
    let srcType;
    if (/^video\//.test(data.mimetype)) {
      srcType = "video";
      setVideo({ src: `http://10.10.10.105:3001/${data.path}` });
    }
    else if (/^audio\//.test(data.mimetype)) {
      srcType = "audio";
      setAudio({ src: `http://10.10.10.105:3001/${data.path}` });
    }
    else if (/^image\//.test(data.mimetype)) {
      srcType = "image";
      setImage({ src: `http://10.10.10.105:3001/${data.path}` });
    }
    else {
      srcType = "unknown";
    }
    for (let element of playerElements) {
      element.dataset.srcType = srcType;
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  Playlist autoplay button
  const disableAutoplay = () => {
    autoplay = false;
    
    for (let element of autoplayElements) {
      element.querySelector(".js__name").innerHTML = "Enable Autoplay";
      element.onclick = () => {
        enableAutoplay();
      };
    };
  };
  
  const enableAutoplay = () => {
    autoplay = true;
    
    for (let element of autoplayElements) {
      element.querySelector(".js__name").innerHTML = "Disable Autoplay";
      element.onclick = () => {
        disableAutoplay();
      };
    };
  };
  
  if (autoplay) {
    enableAutoplay();
  }
  else {
    disableAutoplay();
  }
  
  //////////////////////////////////////////////////////////////////////////////
  //  Playlist next button
  const setNext = ({ path, name }) => {
    for (let element of nextElements) {
      element.querySelector(".js__name").innerHTML = `Next: ${name}`;
      element.onclick = () => {
        history.pushState({}, "", path);
        loadPage({ autoplay, url: path });
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  Playlist prev button
  const setPrev = ({ path, name }) => {
    for (let element of prevElements) {
      element.querySelector(".js__name").innerHTML = `Previous: ${name}`;
      element.onclick = () => {
        history.pushState({}, "", path);
        loadPage({ autoplay, url: path });
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  set video
  const setVideo = ({ src }) => {
    for (let element of videoElements) {
      element.src = src;
      element.load();
      element.play();
      
      element.onended = () => {
        if (autoplay) {
          history.pushState({}, "", nextURL);
          loadPage({ autoplay, url: nextURL, });
        }
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  set video
  const setAudio = ({ src }) => {
    for (let element of audioElements) {
      element.src = src;
      element.load();
      element.play();
      
      element.onended = () => {
        if (autoplay) {
          history.pushState({}, "", nextURL);
          loadPage({ autoplay, url: nextURL, });
        }
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  set video
  const setImage = ({ src }) => {
    for (let element of imageElements) {
      element.setAttribute("src", src);
    }
    imageTimeout = setTimeout(() => {
      if (autoplay) {
        history.pushState({}, "", nextURL);
        loadPage({ autoplay, url: nextURL, });
      }
    }, 5000);
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  load
  loadPage({ 
    autoplay: /0|false/.test((new URL(window.location.href)).searchParams.get("autoplay")) ? false : true, 
    url: window.location.href 
  });
});
