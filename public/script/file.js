var AUTOPLAY_DELAY = 3000;

var SearchParams = function (search) {
  var items = {};
  
  search = search.slice(1);
  var pairs = search.split("&");
  var i;
  var pairsLength = pairs.length;
  for (i = 0; i < pairsLength; ++i) {
    var pair = pairs[i].split("=");
    var key = pair[0];
    var value = pair[1];
    
    items[key] = value;
  }
  
  return {
    set: function (key, value) {
      items[key] = value;
      return this;
    },
    get: function (key) {
      return items[key];
    },
    toString: function () {
      var result = [];
      var key;
      
      for (key in items) {
        var value = items[key];
        
        result.push(key + "=" + value);
      }
      
      return "?" + result.join("&");
    }
  }
};

var URL = function (href) {
  var parseHref = function (href) {
    var split = href.match(/([^:/]+:)\/\/((?:(?:\d{1,3}\.){3}\d{1,3})|(?:[^:]+))(?::(\d+))?(\/[^#?]*)?(\?[^#]+)?(#.*)?/);
    
    if (split === null) {
      throw "Invalid URL";
    }
    else {
      this.protocol = split[1] || "";
      this.hostname = split[2] || "";
      this.port = split[3] || "";
      this.pathname = split[4] || "/";
      this.search = split[5] || "";
      this.hash = split[6] || "";
      this.username = "";
      this.password = "";
      this.host = this.hostname + ":" + this.port;
      this.origin = this.protocol + "//" + this.host;
      this.searchParams = SearchParams(this.search);
    }
  };
  
  var that = this;
  Object.defineProperty(this, "href", {
    configurable: true,
    enumerable: true,
    get: function () {
      return this.protocol + "//" + this.hostname + ":" + this.port + "/" + this.pathname + this.searchParams + this.hash;
    },
    set: function (href) {
      parseHref.call(that, href);
    },
  });
  
  parseHref.call(this, href);
};

window.addEventListener("DOMContentLoaded", function (e) {
  var autoplayElements = document.getElementsByClassName("js-autoplay");
  var nextElements = document.getElementsByClassName("js-next");
  var prevElements = document.getElementsByClassName("js-prev");
  var videoElements = document.getElementsByClassName("js-video");
  var audioElements = document.getElementsByClassName("js-audio");
  var imageElements = document.getElementsByClassName("js-image");
  var nameElements = document.getElementsByClassName("js-name");
  var playerElements = document.getElementsByClassName("js-player");
  
  var autoplay;
  var nextURL;
  var imageTimeout;
  
  var loadPage = function (options) {
    clearTimeout(imageTimeout);
    var autoplay = options.autoplay;
    var url = options.url;
    var fetchURL = new URL(url);
    fetchURL.searchParams.set("request-json", true);
    
    const req = new XMLHttpRequest();
    req.onreadystatechange = function () {
      if (req.readyState === 4) {
        if (req.status === 200) {
          var res = req.responseText;
          var data = JSON.parse(res);
          
          var origin = window.location.origin;
          nextURL = data.nextHref;
          setNext({ 
            path: data.nextHref, 
            name: data.nextName 
          });
          setPrev({ 
            path: data.prevHref, 
            name: data.prevName 
          });
          
          var i, length;
          
          length = videoElements.length;
          for (i = 0; i < length; ++i) {
            videoElements[i].src = "";
          }
          
          length = audioElements.length;
          for (i = 0; i < length; ++i) {
            audioElements[i].src = "";
          }
          
          length = imageElements.length;
          for (i = 0; i < length; ++i) {
            imageElements[i].src = "";
          }
   
          if (autoplay) {
            enableAutoplay();
          }
          else {
            disableAutoplay();
          } 
          
          document.title = data.name;
          length = nameElements.length;
          for (i = 0; i < length; ++i) {
            nameElements[i].textContent = data.name;
          }
    
          var srcType;
          if (/^video\//.test(data.mediaMimetype)) {
            srcType = "video";
            setVideo({ src: data.mediaHref });
          }
          else if (/^audio\//.test(data.mediaMimetype)) {
            srcType = "audio";
            setAudio({ src: data.mediaHref });
          }
          else if (/^image\//.test(data.mediaMimetype)) {
            srcType = "image";
            setImage({ src: data.mediaHref });
          }
          else {
            srcType = "unknown";
          }
          length = playerElements.length;
          for (i = 0; i < length; ++i) {
            playerElements[i].dataset.srcType = srcType;
          }
        }
      }
    };
    req.open("GET", fetchURL.href);
    req.send();
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  Playlist next button
  var setNext = function (options) {
    var path = options.path;
    var name = options.name;
    
    var i;
    var length = nextElements.length;
    for (i = 0; i < length; ++i) {
      var element = nextElements[i];
      element.getElementsByClassName("js__name")[0].innerHTML = "Next: " + name;
      element.onclick = function () {
        history.pushState({}, "", path);
        loadPage({ autoplay: autoplay, url: path });
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  Playlist prev button
  var setPrev = function (options) {
    var path = options.path;
    var name = options.name;
    
    var i;
    var length = prevElements.length;
    for (i = 0; i < length; ++i) {
      var element = prevElements[i];
      element.getElementsByClassName("js__name")[0].innerHTML = "Previous: " + name;
      element.onclick = function () {
        history.pushState({}, "", path);
        loadPage({ autoplay: autoplay, url: path });
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  Playlist autoplay button
  var disableAutoplay = function () {
    autoplay = false;
    
    var i;
    var length = autoplayElements.length;
    for (i = 0; i < length; ++i) {
      var element = autoplayElements[i];
      element.getElementsByClassName("js__name")[0].innerHTML = "Enable Autoplay";
      element.onclick = function () {
        enableAutoplay();
      };
    };
  };
  
  var enableAutoplay = function () {
    autoplay = true;
    
    var i;
    var length = autoplayElements.length;
    for (i = 0; i < length; ++i) {
      var element = autoplayElements[i];
      element.getElementsByClassName("js__name")[0].innerHTML = "Disable Autoplay";
      element.onclick = function () {
        disableAutoplay();
      };
    };
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  set video
  var setVideo = function (options) {
    var src = options.src;
    
    var i;
    var length = videoElements.length;
    for (i = 0; i < length; ++i) {
      var element = videoElements[i];
      element.src = src;
      element.load();
      element.play();
      
      element.onended = function () {
        if (autoplay) {
          history.pushState({}, "", nextURL);
          loadPage({ autoplay: autoplay, url: nextURL, });
        }
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  set video
  var setAudio = function (options) {
  console.log("here");
    var src = options.src;
    
    var i;
    var length = audioElements.length;
    for (i = 0; i < length; ++i) {
      var element = audioElements[i];
      element.src = src;
      element.load();
      element.play();
      
      element.onended = function () {
        if (autoplay) {
          history.pushState({}, "", nextURL);
          loadPage({ autoplay: autoplay, url: nextURL, });
        }
      };
    }
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  set video
  const setImage = function (options) {
    var src = options.src;
    
    var i;
    var length = imageElements.length;
    for (i = 0; i < length; ++i) {
      var element = imageElements[i];
      element.setAttribute("src", src);
    }
    imageTimeout = setTimeout(function () {
      if (autoplay) {
        history.pushState({}, "", nextURL);
        loadPage({ autoplay: autoplay, url: nextURL, });
      }
    }, 5000);
  };
  
  //////////////////////////////////////////////////////////////////////////////
  //  load
  loadPage({ 
    autoplay: /0|false/.test((new URL(window.location.href)).searchParams.get("autoplay")) ? false : true, 
    url: window.location.href 
  });


});
