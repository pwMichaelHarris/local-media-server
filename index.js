////////////////////////////////////////////////////////////////////////////////
//  imports
import { createServer as createClientServer } from "client";
import { createServer as createCDNServer } from "cdn";
import commandLineArgs from "command-line-args";
import commandLineUsage from "command-line-usage";
import { 
  isAbsolute as isAbsolutePath, 
  normalize as normalizePath 
} from "node:path";

////////////////////////////////////////////////////////////////////////////////
//  CLI options
const optionDefinitions = [
  {
    name: "cdn-origin", 
    alias: "c", 
    type: String, 
    defaultValue: "http://127.0.0.1:3001", 
    lazyMultiple: true, 
    typeLabel: "{underline URL}",
    description: "The origin of the CDN server. Multiple server may be used for load balancing. (MULTIPLE SERVER CURRENTLY NOT IMPLEMENTED)" 
  }, { 
    name: "help", 
    type: Boolean, 
    description: "Display this guide." 
  }, { 
    name: "noise", 
    alias: "n", 
    type: Number, 
    defaultValue: "1", 
    description: "How much should the server output.\n0. No output\n1. Output only start up and closing and each request made" 
  }, { 
    name: "origin", 
    alias: "o", 
    type: String, 
    defaultValue: "http://127.0.0.1:3000", 
    lazyMultiple: true, 
    typeLabel: "{underline URL}",
    description: "The origin to be used for the server. Multiple server may be used for load balancing. (MULTIPLE SERVER CURRENTLY NOT IMPLEMENTED)"
  }, { 
    name: "public", 
    alias: "p", 
    type: String, 
    defaultValue: "public", 
    typeLabel: "{underline path}",
    description: "A path to be used for static file serving."  
  }, { 
    name: "root", 
    alias: "r", 
    type: String, 
    lazyMultiple: true, 
    typeLabel: "{underline path}",
    description: "Add a single root path for the sever to use as a base for server on the file system directory" 
  }, { 
    name: "roots", 
    type: String, 
    multiple: true,
    typeLabel: "{underline paths} ...",
    description: "Add multiple root paths" 
  },
];

const options = commandLineArgs(optionDefinitions, { camelCase: true });

////////////////////////////////////////////////////////////////////////////////
//  If help was selected, then just output the options and quit
if (options.help) {
  const usage = commandLineUsage([
    {
      header: "Options",
      optionList: optionDefinitions
    }
  ]);
  
  console.log(usage);
  process.exit(0);
}

////////////////////////////////////////////////////////////////////////////////
//  If help was not selected then build all other values.

//  Where the CDN (content) server is located.
let cdnOrigin, cdnPort, cdnAddress;
try {
  let url = new URL(options.cdnOrigin[0]);
  cdnOrigin = options.cdnOrigin[0];
  cdnAddress = url.hostname;
  cdnPort = Number.parseInt(url.port || "80");
}
catch (e) {
  if (e.code === "ERR_INVALID_URL") {
    console.error("ERROR: The CDN's origin must be a valid URL.");
    process.exit(1);
  }
  else {
    throw e;
  }
}

//  Noise level. Logging is currently just tracking request coming in.
let log;
if (options.noise === "1") {
  log = console.log.bind(log);
} else if (options.noise === "0") {
  log = (...args) => {};
} else {
  console.error("ERROR: Noise level must be 0 or 1.");
  process.exit(1);
}

//  Where the client server is located.
let address, port;
try {
  let url = new URL(options.origin[0]);
  address = url.hostname;
  port = Number.parseInt(url.port || "80");
}
catch (e) {
  if (e.code === "ERR_INVALID_URL") {
    console.error("ERROR: The server's origin must be a valid URL.");
    process.exit(1);
  }
  else {
    throw e;
  }
}

//  The host machine path to public/static files
let publicPath = options.public;
if (!isAbsolutePath(publicPath)) {
  publicPath = normalizePath(`${process.cwd()}/${publicPath}`);
}

//  The root bases for all file system look ups
let roots = [...(options.root ?? []), ...(options.roots ?? [])];
if (roots.length === 0) {
  roots.push(process.cwd());
}
else {
  roots = roots.map(root => {
    if (isAbsolutePath(root)) {
      return root;
    }
    return normalizePath(`${process.cwd()}/${root}`);
  });
}

////////////////////////////////////////////////////////////////////////////////
//  Build the content and client server
const cdnServer = createCDNServer({ 
  origin: cdnOrigin, 
  port: cdnPort, 
  address: cdnAddress, 
  roots, 
  log 
});
const clientServer = createClientServer({ 
  cdnOrigin, 
  log, 
  address, 
  port, 
  publicPath, 
  roots 
});


