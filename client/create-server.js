////////////////////////////////////////////////////////////////////////////////
//  Imports
import { createServer } from "node:http";
import createListener from "./create-listener.js";

////////////////////////////////////////////////////////////////////////////////
//  Creates a simple node server using the listener. Set create-listener.js for 
//    details.
export default ({port, address, roots, log, cdnOrigin, publicPath}) => {
  log = log ?? ((...args) => {});
  
  const origin = `http://${address}:${port}`;

  const listener = createListener({roots, log, cdnOrigin, origin, publicPath});
  const server = createServer(listener);

  server.on("listening", () => {
    log(`${(new Date).toJSON()} Server listening on ${origin} with roots ${roots.map(i => `'${i}'`).join("; ")} and public folder '${publicPath}'`);
    
    process.on("SIGINT", () => {
      server.close();
    });
  });

  server.on("close", () => {
    log(`${(new Date).toJSON()} Server on ${origin} closed`);
    process.exit();
  });

  server.listen(port, address);
  
  return server;
};
