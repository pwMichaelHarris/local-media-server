////////////////////////////////////////////////////////////////////////////////
import createServer from "./create-server.js";
import { request as nodeRequest } from "node:http";

////////////////////////////////////////////////////////////////////////////////
const roots = [
  "/home/pw/Videos",
  "/home/pw/Music",
  "/home/pw/Pictures",
];
const port = 3000;
const address = "127.0.0.1";
const cdnOrigin = "http://127.0.0.1:3001";
const publicPath = `${process.cwd()}/public`;
const log = console.log.bind(console);

////////////////////////////////////////////////////////////////////////////////
const origin = `http://${address}:${port}`;
const paths = [
  "?sort=type",
  "/junk",
  "/fs/no-exists?request-json=1",
  "/fs/eevee?sort=size&request-json=1",
  "/fs/eevee?request-json=0",
  "/fs/classic?request-json=1",
];

////////////////////////////////////////////////////////////////////////////////
const request = (url) => new Promise((resolve, reject) => {
  const req = nodeRequest(url, (res) => {
    const { statusCode, headers } = res;
    const contentType = headers["content-type"];
    
    const buffer = [];
    res.on("data", buffer.push.bind(buffer));
    res.on("end", () => {
      let body = Buffer.concat(buffer)
      if (/application\/json(;|$)/.test(contentType)) {
        body = JSON.parse(body.toString("utf-8"));
      }
      else if (/text\/html(;|$)/.test(contentType)) {
        body = body.toString("utf-8");
      }
      
      resolve({ statusCode, contentType, headers, body });
    });
  });
  req.on("error", reject);
  req.end();
});

////////////////////////////////////////////////////////////////////////////////
const server = createServer({port, address, roots, log, cdnOrigin, publicPath});

for (let path of paths) {
  let url = `${origin}${path}`;
  
  try {
    const result = await request(url);
    
    if (result.statusCode === 200 && /application\/json(;|$)/.test(result.contentType) && result.body.type === "Folder") {
      console.log({ url, ...result, entries: result.body.entries });
    }
    
    else {
      console.log({ url, ...result });
    }
  }
  
  catch (e) {
    console.error({ url, e });
  }
}
server.close();


