////////////////////////////////////////////////////////////////////////////////
//  Imports
import createPage from "./base.js";

////////////////////////////////////////////////////////////////////////////////
//  Return an HTML string containing a rendering of the entry file.
//  entry:  All relevent data for the file. Can be gotten from multiroot-path 
//          module's getEntry function.
export default (entry) => {
  const headlineText = `
  <h2 class="headline js-name">${entry.name}</h2>`;

  const controlsText = `
  <noscript class="controls">
    <a class="control" href="${entry.parentHref}">
      <img class="control__icon">
      <div class="control__name">Parent Folder</div>
    </a>
    <a class="control" href="${entry.homeHref}">
      <img class="control__icon">
      <div class="control__name">Home Folder</div>
    </a>
    <a class="control" href="${entry.prevHref}">
      <img class="control__icon">
      <div class="control__name">Previous: ${entry.prevName}</div>
    </a>
    <a class="control" href="${entry.nextHref}">
      <img class="control__icon">
      <div class="control__name">Next: ${entry.nextName}</div>
    </a>
    <div class="control">
      <img class="control__icon">
      <div class="control__name">JavaScript must be enabled for auto-play</div>
    </div>
  </noscript>
  
  <div class="controls js">
    <a class="control" href="${entry.parentHref}">
      <img class="control__icon">
      <div class="control__name">Parent Folder</div>
    </a>
    <a class="control" href="${entry.homeHref}">
      <img class="control__icon">
      <div class="control__name">Home Folder</div>
    </a>
    <div class="control js-prev">
      <img class="control__icon js__icon">
      <div class="control__name js__name"></div>
    </div>
    <div class="control js-next">
      <img class="control__icon js__icon">
      <div class="control__name js__name"></div>
    </div>
    <div class="control js-autoplay">
      <img class="control__icon js__icon">
      <div class="control__name js__name"></div>
    </div>
  </div>`;
  
  
  const { mediaMimetype: mimetype, mediaHref: mediaSrc } = entry;
  let sourceType;
  let sourceTypeText;
  if (/video\//.test(mimetype)) {
    sourceType = sourceTypeText = "video";
  }
  else if (/audio\//.test(mimetype)) {
    sourceType = sourceTypeText = "audio";
  }
  else if (/image\//.test(mimetype)) {
    sourceType = sourceTypeText = "image";
  }
  else {
    sourceTypeText = "unknown";
  }
  
  const playerText = `
  <div class="player js-player" data-src-type="${sourceTypeText}">
    <audio class="js-audio player__audio" autoplay controls>
      <source ${sourceType === "audio" ? `src="${entry.mediaHref}" ` : ""}/>
    </audio>
    
    <video class="player__video js-video" autoplay controls>
      <source ${sourceType === "video" ? `src="${entry.mediaHref}" ` : ""}/>
    </video>
    
    <img class="player__image js-image" ${sourceType === "image" ? `src="${entry.mediaHref}" ` : ""}/>
  </div>`;

  const content = `${headlineText}${playerText}${controlsText}`;

  return createPage({
    title: entry.name,
    styles: [{href: "/style/file.css"}],
    scripts: [{src: "/script/file.js"}],
    content,
  });
};
