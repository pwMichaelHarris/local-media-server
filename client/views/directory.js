////////////////////////////////////////////////////////////////////////////////
//  Imports
import { normalize as normalizePath } from "node:path";
import createPage from "./base.js";
import { getEntry } from "multiroot-path";
import prettySize from "./helpers/pretty-size.js";

////////////////////////////////////////////////////////////////////////////////
//  Return an HTML string containing a rendering of the entry directory.
//  entry:  All relevent data for the directory. Can be gotten from 
//          multiroot-path module's getEntry function.
export default (entry) => {
  const headlineText = `<h2 class="headline">
    <div class="headline--path">${entry.path}</div>
    <div class="headline--name">${entry.name}</div>
  </h2>`;

  const controlsText = `<div class="controls">
      <a class="control" href="${entry.parentHref}">
        <img class="control__icon">
        <div class="control__name">Parent Folder</div>
      </a>
      <a class="control" href="${entry.homeHref}">
        <img class="control__icon">
        <div class="control__name">Home Folder</div>
      </a>
    </div>`;
    
  const childrenText = entry.children.length === 0
    ? ""
    : "\n" 
      + entry.children
        .map(child => `    <a class="entry" href="${child.href}">
      
      <div class="entry__name">
        <img class="entry__name__icon">
        <div class="entry__name__name">${child.name}</div>
      </div>
      <div class="entry__size">${prettySize(child)}</div>
      <div class="entry__type">${child.type}</div>
    </a>`)
        .join("\n");


  const content = `${headlineText}${controlsText}
  <div class="entries">
    <a class="entry--header">
      <div class="entry__name--header">Name</div>
      <div class="entry__size--header">Size</div>
      <div class="entry__type--header">Type</div>
    </a>${childrenText}
  </div>\n`;

  return createPage({
    title: entry.name,
    styles: [{href: "/style/directory.css"}],
    scripts: [{src: "/script/directory.js"}],
    content,
  });
};
