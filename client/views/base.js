//  Just the basic HTML set-up for the pages.
export default (options) => {
  let {
    title = "Untitled",
    scripts = [],
    styles = [],
    content = "",
  } = options ?? {};
  
  let stylesText;
  if (styles.length === 0) {
    stylesText = "";
  } 
  else {
    stylesText = "\n" + styles
      .map(style => `  <link rel="stylesheet" type="text/css" href="${style.href}">`)
      .join("\n");
  }
  
  let scriptsText;
  if (scripts.length === 0) {
    scriptsText = "";
  } 
  else {
    scriptsText = "\n" + scripts
      .map(script => `  <script src="${script.src}"></script>`)
      .join("\n");
  }
  
  if (content !== "") {
    content = content;
  }
  
  return `<!DOCTYPE html>

<html lang=en>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>${title}</title>
  <link rel="stylesheet" type="text/css" href="/style/reset.css">${stylesText}
  <noscript>
    <link rel="stylesheet" type="text/css" href="/style/noscript.css">
  </noscript>${scriptsText}
</head>

<body>${content}</body>

</html>`;
};
