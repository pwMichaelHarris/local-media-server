////////////////////////////////////////////////////////////////////////////////
const prettyItemSize = (size) => {
  if (size === 1) {
    return "1 item";
  }
  else {
    return `${size.toLocaleString("en-US")} items`;
  }
};

////////////////////////////////////////////////////////////////////////////////
const prettyByteSize = (size) => {
  if (size === 1) {
    return "1 byte";
  }
  else if (size < 1000) {
    return `${size} bytes`;
  }
  else if (size < 1000000) {
    return `${(size / 1000).toFixed(1)} kB`;
  }
  else if (size < 1000000000) {
    return `${(size / 1000000).toFixed(1)} MB`;
  }
  else if (size < 1000000000000) {
    return `${(size / 1000000000).toFixed(1)} GB`;
  }
  else {
    return `${(size / 1000000000000).toFixed(1)} TB`;
  }
};

////////////////////////////////////////////////////////////////////////////////
//  Returns a string of the size adjusted to be prettier looking.
//  IE.
//    size = 1032 and sizeUnit = "items"
//      => "1,032 items"
//    size = 1030332 and sizeUnit = "bytes"
//      => "1.0 MB"
export default ({ size, sizeUnit }) => {
  if (sizeUnit === "items") {
    return prettyItemSize(size);
  }
  else if (sizeUnit === "bytes") {
    return prettyByteSize(size);
  }
  else {
    return `${size} ${sizeUnit}`;
  }
};
