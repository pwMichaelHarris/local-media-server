////////////////////////////////////////////////////////////////////////////////
//  Imports
import createPage from "./base.js";

////////////////////////////////////////////////////////////////////////////////
//  Common HTTP status codes to status messages
const statusMessage = {
  404: "Not Found",
  500: "Internal Server Error",
  501: "Not Implemented",
};

////////////////////////////////////////////////////////////////////////////////
//  Return an HTML string containing a rendering of the entry error.
//  entry:  All relevent data for the error. Can be gotten from multiroot-path 
//          module's getEntry function.
export default (entry) => {
  const headline = `
  <h1 class="headline">Error (${entry.status}): ${statusMessage[entry.status]}</h1>
  <h2 class="sub-headline">${entry.path}</h2>`;
  
  const controls = `
  <div class="controls">
    <a class="control" href="${entry.homeHref}">
      <img class="control__icon">
      <div class="control__name">Go to Home Folder</div>
    </a>
  </div>`;

  const content = `${headline}${controls}`;

  return createPage({
    title: `Not Found: ${entry.path}`,
    styles: [{href: "/style/error.css"}],
    content: content,
  });
};
