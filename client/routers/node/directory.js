////////////////////////////////////////////////////////////////////////////////
//  Imports
import mapMimeToType from "./helpers/mimetype.js";
import createDirectoryPage from "../../views/directory.js";
import { getEntry } from "multiroot-path";

////////////////////////////////////////////////////////////////////////////////
//  Renders a file page for a GET request.
//  res:              The request's res object
//  paramRequestJSON: Should a simple JSON response be used
//  roots:            The site's roots for querying for nodes
//  path:             The request's path to a node
//  compare:          The compare function sorting the parent's children. Used
//                    for getting the next and previous sibling
//  fsOrigin:         The origin of client server
export const render = async ({ res, paramRequestJSON, roots, path, compare, fsOrigin }) => {
  ////////
  const entry = await getEntry( roots, path );
  
  ////////
  //  Get all children
  let children = await Promise.all(entry.children.map(path => getEntry(roots, path)));
  children = children.sort(compare);
  children = children.map(child => ({
    type: mapMimeToType(child.mimetype),
    
    href: `${fsOrigin}${child.path}`,
    path: child.path,
    name: child.name,
    
    size: child.size,
    sizeUnit: child.sizeUnit,
    mimetype: child.mimetype,
  }));

  ////////
  let responseJSON = {
    type: entry.type,
    
    href: `${fsOrigin}${entry.path}`,
    path: entry.path,
    name: entry.name,
    
    homeHref: `${fsOrigin}/`,
    homeName: `/`,
    
    parentHref: `${fsOrigin}${entry.parentPath}`,
    parentName: entry.parentName,
    
    children,
  };
  
  ////////
  //  Render JSON or HTML representation
  if (paramRequestJSON) {
    res
      .type("application/json")
      .status(200)
      .send(responseJSON);
  }
  
  else {
    res
      .type("text/html")
      .status(200)
      .send(createDirectoryPage(responseJSON));
  }
};
