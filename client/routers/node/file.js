////////////////////////////////////////////////////////////////////////////////
//  Imports
import createFilePage from "../../views/file.js";
import { getEntry } from "multiroot-path";

////////////////////////////////////////////////////////////////////////////////
//  Renders a file page for a GET request.
//  base:             A basic summary of the node
//  res:              The request's res object
//  paramRequestJSON: Should a simple JSON response be used
//  roots:            The site's roots for querying for nodes
//  path:             The request's path to a node
//  compare:          The compare function sorting the parent's children. Used
//                    for getting the next and previous sibling
//  fsOrigin:         The origin of client server
//  cdnOrigin:        The origin of CDN (content) server
export const render = async ({ base, res, paramRequestJSON, roots, path, compare, fsOrigin, cdnOrigin }) => {
  ////////
  const entry = await getEntry( roots,  path );
  const parent = await getEntry( roots, entry.parentPath );
  
  ////////
  //  Get all sibilings (AKA parents children)
  let siblings = parent.children;
  siblings = siblings.map(path => getEntry(roots, path));
  siblings = await Promise.all(siblings);
  siblings = siblings.filter(({mimetype}) => /^(audio|video|image)\//.test(mimetype));
  siblings = siblings.sort(compare);
  const index = siblings.findIndex(sibling => sibling.path === entry.path);
  const prev = siblings[(index === 0 || index === -1) ? siblings.length - 1 : index - 1];
  const next = siblings[index === -1 ? 0 : (index + 1) % siblings.length];
  
  ////////
  let responseJSON = {
    type: entry.type,
    
    href: `${fsOrigin}${entry.path}`,
    path: entry.path,
    name: entry.name,
    
    homeHref: `${fsOrigin}/`,
    homeName: `/`,
    
    parentHref: `${fsOrigin}${entry.parentPath}`,
    parentName: entry.parentName,
    
    mediaHref: `${cdnOrigin}${entry.path}`,
    mediaSize: entry.size,
    mediaSizeUnit: entry.sizeUnit,
    mediaMimetype: entry.mimetype,
    
    nextHref: siblings.length === 0 ? undefined : `${fsOrigin}${next.path}`,
    nextName: siblings.length === 0 ? undefined : next.name,
    
    prevHref: siblings.length === 0 ? undefined : `${fsOrigin}${prev.path}`, 
    prevName: siblings.length === 0 ? undefined : prev.name,
  };
  
  ////////
  //  Render JSON version; specialized HTML version for video, audio and image 
  //    media types; or just forward the file there
  if (paramRequestJSON) {
    res
      .type("application/json")
      .status(200)
      .send(responseJSON);
  }
  
  else if (/^(video|audio|image)\//.test(entry.mimetype)) {
    res
      .type("text/html")
      .status(200)
      .send(createFilePage(responseJSON));
  }
  
  else {
    //  TODO: CDN should send files?
    res.sendFile(
      base.rootedPath, 
    
      {
        headers: {
          "content-type": entry.mimetype,
        }
      },
    
      (err) => {
        if (!res.headersSent && err && typeof(err) === "object" && err !== null && err.code === "ENONENT") {
          res.sendStatus(500);
        }
      }
    );
  }

};
