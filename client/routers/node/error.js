////////////////////////////////////////////////////////////////////////////////
//  Imports
import mapMimeToType from "./helpers/mimetype.js";
import createErrorPage from "../../views/error.js";
import { getEntry, getBase } from "multiroot-path";

////////////////////////////////////////////////////////////////////////////////
//  Renders a error page for a GET request.
//  res:              The request's res object
//  paramRequestJSON: Should a simple JSON response be used
//  base:             A basic summary of the node
//  fsOrigin:         The origin of client server
export const render = async ({ res, paramRequestJSON, base, fsOrigin }) => {
  const statusCode = (base.type === "error" && base.code === "no-entry") ? 404 : 500;
  const responseJSON = {
    href: `${fsOrigin}${base.path}`,
    path: base.path,
    
    homeHref: `${fsOrigin}/`,
    homeName: `/`,
    
    status: statusCode,
  };
  
  if (paramRequestJSON) {
    res
      .type("application/json")
      .status(statusCode)
      .send(responseJSON);
  } else {
    res
      .type("text/html")
      .status(statusCode)
      .send(createErrorPage(responseJSON));
  }
};
