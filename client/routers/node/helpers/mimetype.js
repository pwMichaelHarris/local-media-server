const majorMap = new Map([
  ["text", "Text"],
  ["application", "Document"],
  ["audio", "Audio"],
  ["video", "Video"],
  ["image", "Image"],
]);

export default (mimetype) => {
  const majortype = mimetype === undefined ? undefined : mimetype.split("/")[0];
  
  if (mimetype === "inode/directory") {
    return "Folder";
  }
  
  else if (majorMap.has(majortype)) {
    return majorMap.get(majortype);
  }
  
  else {
    return "Error";
  }
};
