////////////////////////////////////////////////////////////////////////////////
//  Imports
import express from "express";
import { normalize as normalizePath, } from "node:path";
import { URL } from "node:url";
import { getBase } from "multiroot-path";
import createSort from "./helpers/sort.js";

import { render as renderDirectory } from "./node/directory.js";
import { render as renderFile } from "./node/file.js";
import { render as renderError } from "./node/error.js";

////////////////////////////////////////////////////////////////////////////////
//  Returns an express router that will attempt to serve summary of a node from 
//    the host machine
//  roots:    An array of base path for the a GET request's path to find a node 
//            on the host machine.
//  cdnOrigin: A origin to the CDN (content) server, needed for the client to 
//             recieve the actual node content.
export const init = ({ roots, cdnOrigin }) => {
  //////////////////////////////////////
  const serveNode = async (req, res, next) => {
    ////////
    //  Get the path
    const origin = `${req.protocol}://${req.get("host")}`;
    const fsOrigin = `${origin}/fs`;
    const {pathname: unescapedPath, searchParams} = new URL(`${origin}${req.url}`);
    const unbasedPath = unescapedPath.replace(/^\/+fs\/*/, "/");
    const path = normalizePath(unescape(unbasedPath)); 
    
    ////////
    //  Get the sort order
    const paramSort = searchParams.get("sort") ?? undefined;
    const paramSeparateByType = /1|true/.test(searchParams.get("separate-by-type") ?? "1") ? true : false;
    const paramOrder = searchParams.get("order") ?? undefined;
    const paramRequestJSON = /1|true/.test(searchParams.get("request-json") ?? "0") ? true : false;
    const compare = createSort({ 
      sort: paramSort,
      separatedByType: paramSeparateByType,
      order: paramOrder,
    });
    
    ////////
    const base = await getBase(roots, path);
    
    ////////
    if (base.type === "directory") {
      renderDirectory({ res, paramRequestJSON, roots, path, compare, fsOrigin });
    }
    else if (base.type === "file") {
      renderFile({ res, base, paramRequestJSON, roots, path, compare, fsOrigin, cdnOrigin });
    }
    else {
      renderError({ res, paramRequestJSON, base, fsOrigin });
    }
  };
  
  //////////////////////////////////////
  const router = express.Router();
  
  router.get( "*", serveNode );
  
  return router;
};
