////////////////////////////////////////////////////////////////////////////////
//  Imports
import express from "express";
import { normalize as normalizePath, } from "node:path";
import { URL } from "node:url";

////////////////////////////////////////////////////////////////////////////////
//  Returns an express router that will attempt to serve static files from the 
//    servers public directory.
//  publicPath: An host machine absolute path to the public folder for static 
//              files.
export const init = ({ publicPath }) => {

  //////////////////////////////////////
  const serveStatic = async (req, res, next) => {
    //  Get the URL's path
    const origin = `${req.protocol}://${req.get("host")}`;
    const {pathname: unescapedPath, searchParams} = new URL(`${origin}${req.originalUrl}`);
    const unbasedPath = unescapedPath.replace(/^\/+fs\/*/, "/");
    const path = normalizePath(unescape(unbasedPath));
    
    //  Serve the file
    res.sendFile(`${publicPath}/${path}`, (err) => {
      if (!res.headersSent) {
        if (err && typeof(err) === "object" && err !== null && err.code === "ENOENT") {
          res.sendStatus(404);
        }
        else {
          res.sendStatus(500);
        }
      }
    });
  };
  
  //////////////////////////////////////
  const router = express.Router();
  
  router.get( "*", serveStatic );
  
  return router;
};
