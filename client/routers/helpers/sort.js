////////////////////////////////////////////////////////////////////////////////
//  Imports
import { createChain, createOrderedCompare, createStringCompare } from "./compare.js";

////////////////////////////////////////////////////////////////////////////////
//  Returns a sort that will sort entries based on their major type 
//    ["directory", "file", "error", etc...] then will sort them by their size, 
//    mime-type, or name. If size or mime-type are choosen then they will first
//    be sorted by that trait and then sub sorted by name.
//  sort:           What attribute should the be the major sort
//  separateByType: Should the major types (ie "directory" and "file") be 
//                  intermixed
//  order:          Should the sort be ascending or descending.
export default ({ sort, separateByType, order }) => {
  let chain = createChain();
  let negateCompare = order === undefined || order == "ascend" ? 1 : -1;
  
  if (separateByType === undefined) {
    const unmappedCompare = createOrderedCompare("directory", "file", "error");
    const mappedCompare = ({type: i}, {type: j}) => unmappedCompare(i, j);
    chain.appendLinks(mappedCompare);
  }
  
  if (sort === "size") {
    chain.appendLinks(({size: i}, {size: j}) => negateCompare * (i - j));
  }
      
  else if (sort === "type") {
    const unmappedCompare = createStringCompare({ caseInsentive: true });
    const mappedCompare = ({mimetype: i}, {mimetype: j}) => {
      i = i === undefined ? "" : i.split("/")[0];
      j = j === undefined ? "" : j.split("/")[0];
      return negateCompare * unmappedCompare(i, j);
    };
    chain.appendLinks(mappedCompare);
  }
  
  //  Add name sort regardless
  const unmappedCompare = createStringCompare({ caseInsentive: true });
  const mappedCompare = ({name: i}, {name: j}) => negateCompare * unmappedCompare(i, j);
  chain.appendLinks(mappedCompare);
  
  return chain;
};

