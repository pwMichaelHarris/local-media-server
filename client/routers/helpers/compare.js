////////////////////////////////////////////////////////////////////////////////
//  Creates a string compare that can used as a standard positive, negative, or 
//    zero compare sort.
//  options.caseInsentive:  Should the case be considered in the comparision. 
//                          Defaults to false.
//  options.ascend:         Should the comparison be ascensing or descending 
//                          order. Defaults to true.
export const createStringCompare = (options) => {
  let {caseInsentive = false, ascend = true} = options ?? {};
  let [ ifGreaterThan, ifLessThan ] = ascend ? [1, -1] : [-1, 1];
  
  if (caseInsentive) {
    return (i, j) => {
      i = i.toLowerCase();
      j = j.toLowerCase();
      
      return i === j ? 0 : (i > j ? ifGreaterThan : ifLessThan);
    };
  }
  else {
    return (i, j) => i === j ? 0 : (i > j ? ifGreaterThan : ifLessThan);
  }
};

////////////////////////////////////////////////////////////////////////////////
//  Creates an ordering of enumated items.
//  IE: createOrderedCompare("foo", "bar", "qux") will return a function that 
//      would compare:
//        "foo" > "bar"
//        "qux" < "foo"
//        "asdf" < "bar"
export const createOrderedCompare = (...items) => {
  let order = new Map(items.map((i, j) => [i, j]));
  let getOrder = (key) => order.has(key) ? order.get(key) : (appendUnknown ? Infinity : -Infinity);
  let orderLength = order.size;
  let reversed = false;
  let appendUnknown = true;

  const result = (i, j) => (getOrder(i) - getOrder(j)) * (reversed ? -1 : 1);
  
  result.reversed = function (val) {
    if (val === undefined) {
      return reversed;
    }
    reversed = !!val;
    return this;
  };
  
  return result;
};

////////////////////////////////////////////////////////////////////////////////
//  Creates a chain of comparisons that will be executed sequentially until a 
//    comparision link results in a non-equal (non-zero) result.
export const createChain = (...compares) => {
  const result = (i, j) => {
    for (let compare of compares) {
      const result = compare(i, j);
      if (result !== 0) {
        return result;
      }
    }
    return 0;
  };
  
  result.appendLinks = (...links) => {
    compares.push(...links);
  };
  
  result.prependLinks = (...links) => {
    compares = [...links, ...compares];
  };
  
  result.removeLinks = (...links) => {
    const removedLink = Set.prototype.has.bind(new Set(links));
    compares = compares.filter(compare => !removedLink(compare));
  }
  
  result.clearLinks = () => {
    compares.length = 0;
  };
  
  return result;
}
