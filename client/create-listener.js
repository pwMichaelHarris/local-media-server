////////////////////////////////////////////////////////////////////////////////
//  imports
import express from "express";

import { init as initStaticRouter } from "./routers/static.js";
import { init as initNodeRouter } from "./routers/node.js";

////////////////////////////////////////////////////////////////////////////////
//  Creates a server request handler.
//    log:        This function will be called to output all information from 
//                an incoming request. It defaults to a silent ignore.
//    roots:      An array of string paths. The roots are the base of every URL 
//                path that accesses the file system.
//    origin:     The URL origin that the current server is using
//    cdnOrigin:  The URL origin for getting requested file content
//    publicPath: An absolute path on the host machine leading to a folder 
//                containing all static files for the site. The client side 
//                scripts, styles, and images are located here.
export default ({ roots, log = (...args) => {}, origin, cdnOrigin, publicPath }) => {
  const app = express();
  
  //////////////////////////////////////
  const staticRouter = initStaticRouter({ publicPath });
  const nodeRouter = initNodeRouter({ roots, cdnOrigin });

  //////////////////////////////////////
  //  General logging
  app.all("*", (req, res, next) => {
    log(`${(new Date()).toJSON()} Request on ${req.protocol}://${req.get("host")}${req.path}`);
    next();
  });
  
  //////////////////////////////////////
  app.use( "/+script|/+style|/+image",  staticRouter );
  app.use( [/^\/+$/, "/+fs"],           nodeRouter   );

  return app;
};

