////////////////////////////////////////////////////////////////////////////////
//  imports
import { normalize as normalizePath, basename } from "node:path";
import { readdir, stat } from "node:fs/promises";
import { lookup as getMimetypeByExtension } from "mime-types";
import getBase from "./get-base.js";

////////////////////////////////////////////////////////////////////////////////
//  Get the directory's summary (name, size, parent basic summary, and 
//    children's basic summary.
//  roots:  An array of string paths. Each root will act as a base for the path
//  path:   The path that should be checked relative to every root.
//  Example:
//    roots = ["foo", "bar"]
//    path = "qux"
//    Paths to be checked will be "foo/qux" and "bar/qux"
const getDirectory = async (roots, base) => {
  const parentPath = normalizePath(`${base.path}/..`);
  const parentName = basename(parentPath) || "/";
  const name = basename(base.path) || "/";
  
  //  Get all the children base information
  let children = base.rootedPaths.map(rootedPath => readdir(rootedPath));
  children = await Promise.all(children);
  children = children.flat();
  children = [...(new Set(children))];
  children = children.map(path => normalizePath(`${base.path}/${path}`));

  return {
    ...base,
    size: children.length,
    sizeUnit: "items",
    parentPath,
    parentName,
    name,
    children,
    mimetype: "inode/directory",
  };
};

////////////////////////////////////////////////////////////////////////////////
//  Get the file's summary (name, size, parent basic summary, mimetype.
const getFile = async (roots, base) => {
  const parentPath = normalizePath(`${base.path}/..`);
  const parentName = basename(parentPath) || "/";
  const name = basename(base.path) || "/";
  
  return {
    ...base,
    size: (await stat(base.rootedPath)).size,
    sizeUnit: "bytes",
    mimetype:    getMimetypeByExtension(base.rootedPath) || "application/octet-stream",
    parentPath,
    parentName,
    name,
  };
};

////////////////////////////////////////////////////////////////////////////////
//  Resolves the path regardless of node type. Returns the node's name, size,
//    parent basic information, mime-type, child basic information as \
//    applicable.
const getEntry = async (roots, path) => {
  const base = await getBase(roots, path);
  
  if (base.type === "directory") {
    return getDirectory(roots, base);
  }
  
  else if (base.type === "file") {
    return getFile(roots, base);
  }
  
  else if (base.type === "error") {
    return base;
  }
  
  else {
    return {
      type: "error",
      reason: "Unrecognized type",
      code: "unknown-type",
      params: [roots, path],
      base,
    }
  }
};

////////////////////////////////////////////////////////////////////////////////
//  exports
export default getEntry;

