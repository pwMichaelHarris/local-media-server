////////////////////////////////////////////////////////////////////////////////
//  imports
import { stat } from "node:fs/promises";
import { normalize as normalizePath } from "node:path";

////////////////////////////////////////////////////////////////////////////////
//  Gets a basic summary of a node found a path. The summary includes its name
//    and rooted path.
//  roots:  An array of string paths. Each root will act as a base for the path
//  path:   The path that should be checked relative to every root.
//  Example:
//    roots = ["foo", "bar"]
//    path = "qux"
//    Paths to be checked will be "foo/qux" and "bar/qux"
const getBase = async (roots, path) => {
  //  Get the meta data for any node located at root/path.
  const getRootMeta = async (root) => {
    const rootedPath = normalizePath(`${root}/${absolutePath}`);
    
    try {
      const stats = await stat(rootedPath, "r");
      if (stats.isDirectory()) {
        return { path: rootedPath, type: "directory", };
      }
      else if (stats.isFile()) {
        return { path: rootedPath, type: "file", };
      }
      else {
        return { path: rootedPath, type: "error", reason: "unknown file type" }
      }
    } catch (e) {
      if (e.code === "ENOENT") {
        return { path: rootedPath, type: "no-entry" };
      }
      else {
        return { path: rootedPath, type: "error", reason: e };
      }
    }
  };

  //  Prevent the path from leaving the root. IE: path = "../foo/"
  const absolutePath = normalizePath(`/${path}`);
  
  //  Common information regardless of what type of node is discovered
  const base = {
    path: absolutePath,
  };
  
  //  Resolve all root/path combinations
  const nodes = (await Promise.all(roots.map(getRootMeta)))
    .filter(({type}) => type !== "no-entry");
  
  //  If there are no entries
  if (nodes.length === 0) {
    return {
      ...base,
      type: "error",
      reason: "no entry",
      code: "no-entry",
      nodes,
    };
  }
  
  //  If there's an error entries
  else if (nodes.some(({type}) => type === "error")) {
    return {
      ...base,
      type: "error",
      reason: "Error while processing paths",
      code: "node-error",
      nodes,
    };
  }
  
  //  If there's only directory entries
  else if (nodes.every(({type}) => type === "directory")) {
    return {
      ...base,
      type: "directory",
      rootedPaths: nodes.map(({path}) => path),
    };
  }
  
  //  If there's only one file entry
  else if (nodes.length === 1 && nodes[0].type === "file") {
    return {
      ...base,
      type: "file",
      rootedPath: nodes[0].path,
    };
  }
  
  else {
    return {
      ...base,
      type: "error",
      reason: "ambiguous node type",
      code: "ambiguous-type",
      nodes,
    };
  }
};

////////////////////////////////////////////////////////////////////////////////
//  exports
export default getBase;

