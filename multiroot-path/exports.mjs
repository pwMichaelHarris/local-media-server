import getBase from "./get-base.js";
import getEntry from "./get-entry.js";

export { getBase, getEntry };
